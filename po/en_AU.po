# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# John Humphrys <john.humphrys@pm.me>, 2018
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-03-23 06:37-0400\n"
"PO-Revision-Date: 2018-07-13 15:34+0000\n"
"Last-Translator: John Humphrys <john.humphrys@pm.me>, 2018\n"
"Language-Team: English (Australia) (https://www.transifex.com/xfce/teams/16840/en_AU/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: en_AU\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ../xfce4-panel-profiles.desktop.in.h:1
#: ../xfce4-panel-profiles/xfce4-panel-profiles.py:51
msgid "Panel Profiles"
msgstr "Panel Profiles"

#: ../xfce4-panel-profiles.desktop.in.h:2
msgid "Backup and restore your panel configuration"
msgstr "Backup and restore your panel configuration"

#: ../xfce4-panel-profiles/xfce4-panel-profiles.glade.h:1
msgid ""
"<b><big>Panel Profiles</big></b>\n"
"Backup and restore your panel configuration"
msgstr ""
"<b><big>Panel Profiles</big></b>\n"
"Backup and restore your panel configuration"

#: ../xfce4-panel-profiles/xfce4-panel-profiles.glade.h:3
msgid "Filename"
msgstr "Filename"

#: ../xfce4-panel-profiles/xfce4-panel-profiles.glade.h:4
msgid "Date Modified"
msgstr "Date Modified"

#: ../xfce4-panel-profiles/xfce4-panel-profiles.glade.h:5
msgid "Apply Configuration"
msgstr "Apply Configuration"

#: ../xfce4-panel-profiles/xfce4-panel-profiles.glade.h:6
#: ../xfce4-panel-profiles/xfce4-panel-profiles.py:293
msgid "Save Configuration"
msgstr "Save Configuration"

#: ../xfce4-panel-profiles/xfce4-panel-profiles.glade.h:7
msgid "Remove Configuration"
msgstr "Remove Configuration"

#: ../xfce4-panel-profiles/xfce4-panel-profiles.glade.h:8
msgid "Export"
msgstr "Export"

#: ../xfce4-panel-profiles/xfce4-panel-profiles.glade.h:9
msgid "Import"
msgstr "Import"

#: ../xfce4-panel-profiles/xfce4-panel-profiles.glade.h:10
msgid "Save panel configuration as..."
msgstr "Save panel configuration as..."

#: ../xfce4-panel-profiles/xfce4-panel-profiles.glade.h:11
#: ../xfce4-panel-profiles/xfce4-panel-profiles.py:85
#: ../xfce4-panel-profiles/xfce4-panel-profiles.py:292
msgid "Cancel"
msgstr "Cancel"

#: ../xfce4-panel-profiles/xfce4-panel-profiles.glade.h:12
msgid "<b>Name the new panel configuration</b>"
msgstr "<b>Name the new panel configuration</b>"

#: ../xfce4-panel-profiles/xfce4-panel-profiles.py:80
msgid "Save"
msgstr "Save"

#: ../xfce4-panel-profiles/xfce4-panel-profiles.py:82
msgid "Open"
msgstr "Open"

#: ../xfce4-panel-profiles/xfce4-panel-profiles.py:140
msgid "Current Configuration"
msgstr "Current Configuration"

#: ../xfce4-panel-profiles/xfce4-panel-profiles.py:160
msgid "Today"
msgstr "Today"

#: ../xfce4-panel-profiles/xfce4-panel-profiles.py:162
msgid "Yesterday"
msgstr "Yesterday"

#: ../xfce4-panel-profiles/xfce4-panel-profiles.py:187
#, python-format
msgid "%s (Copy of %s)"
msgstr "%s (Copy of %s)"

#: ../xfce4-panel-profiles/xfce4-panel-profiles.py:211
msgid "Export configuration as..."
msgstr "Export configuration as..."

#: ../xfce4-panel-profiles/xfce4-panel-profiles.py:212
msgid "Untitled"
msgstr "Untitled"

#: ../xfce4-panel-profiles/xfce4-panel-profiles.py:224
msgid "Import configuration file..."
msgstr "Import configuration file..."

#: ../xfce4-panel-profiles/xfce4-panel-profiles.py:285
msgid "Name the new panel configuration"
msgstr "Name the new panel configuration"

#: ../xfce4-panel-profiles/xfce4-panel-profiles.py:309
#, python-format
msgid "Backup_%s"
msgstr "Backup_%s"

#: ../data/metainfo/org.xfce.PanelProfiles.appdata.xml.in.h:1
msgid "Application to manage different panel layouts in Xfce"
msgstr "Application to manage different panel layouts in Xfce"

#: ../data/metainfo/org.xfce.PanelProfiles.appdata.xml.in.h:2
msgid ""
"Multitude of panel layouts can be created using Xfce panel. This tool "
"enables managing different layouts with little effort. Xfpanel-switch makes "
"it possible to backup, restore, import and export panel layouts."
msgstr ""
"Multitude of panel layouts can be created using Xfce panel. This tool "
"enables managing different layouts with little effort. Xfpanel-switch makes "
"it possible to backup, restore, import and export panel layouts."
